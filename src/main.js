// main.js

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import store from './store'
import AOS from "aos";
import "aos/dist/aos.css";
import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);


Vue.config.productionTip = false

new Vue({
  created() {
    AOS.init();
  },
  router,
  vuetify,
  store,
  render: h => h(App),
}).$mount('#app')
